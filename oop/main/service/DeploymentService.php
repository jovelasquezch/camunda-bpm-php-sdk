<?php


namespace org\camunda\php\sdk\service;

use Exception;
use org\camunda\php\sdk\entity\request\DeploymentRequest;
use org\camunda\php\sdk\entity\response\Deployment;

class DeploymentService extends RequestService
{
    /**
     * Create a Deployment
     *
     * @param DeploymentRequest $request
     * @return void
     */
    public function create($request)
    {
        $deployment = new Deployment();
        $this->setRequestUrl("/deployment/create");
        $this->setRequestObject($request);
        $this->setRequestMethod('POST');

        try {
            return $deployment->cast($this->executeUploadFile());
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function redeploy()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function list()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getListCount()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function get()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getResources()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getResource()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getResourceBinary()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function delete()
    { }
}
