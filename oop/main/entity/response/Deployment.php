<?php

namespace org\camunda\php\sdk\entity\response;

use org\camunda\php\sdk\helper\CastHelper;

class Deployment extends CastHelper
{
    protected $id;
    protected $name;
    protected $source;
    protected $deploymentTime;
    protected $tenantId;
    protected $deployedProcessDefinitions;
    protected $deployedCaseDefinitions;
    protected $deployedDecisionDefinitions;
    protected $deployedDecisionRequirementsDefinitions;

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set the value of source
     *
     * @return  self
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get the value of deploymentTime
     */
    public function getDeploymentTime()
    {
        return $this->deploymentTime;
    }

    /**
     * Set the value of deploymentTime
     *
     * @return  self
     */
    public function setDeploymentTime($deploymentTime)
    {
        $this->deploymentTime = $deploymentTime;

        return $this;
    }

    /**
     * Get the value of tenantId
     */
    public function getTenantId()
    {
        return $this->tenantId;
    }

    /**
     * Set the value of tenantId
     *
     * @return  self
     */
    public function setTenantId($tenantId)
    {
        $this->tenantId = $tenantId;

        return $this;
    }

    /**
     * Get the value of deployedProcessDefinitions
     */
    public function getDeployedProcessDefinitions()
    {
        return $this->deployedProcessDefinitions;
    }

    /**
     * Set the value of deployedProcessDefinitions
     *
     * @return  self
     */
    public function setDeployedProcessDefinitions($deployedProcessDefinitions)
    {
        $this->deployedProcessDefinitions = $deployedProcessDefinitions;

        return $this;
    }

    /**
     * Get the value of deployedCaseDefinitions
     */
    public function getDeployedCaseDefinitions()
    {
        return $this->deployedCaseDefinitions;
    }

    /**
     * Set the value of deployedCaseDefinitions
     *
     * @return  self
     */
    public function setDeployedCaseDefinitions($deployedCaseDefinitions)
    {
        $this->deployedCaseDefinitions = $deployedCaseDefinitions;

        return $this;
    }

    /**
     * Get the value of deployedDecisionDefinitions
     */
    public function getDeployedDecisionDefinitions()
    {
        return $this->deployedDecisionDefinitions;
    }

    /**
     * Set the value of deployedDecisionDefinitions
     *
     * @return  self
     */
    public function setDeployedDecisionDefinitions($deployedDecisionDefinitions)
    {
        $this->deployedDecisionDefinitions = $deployedDecisionDefinitions;

        return $this;
    }

    /**
     * Get the value of deployedDecisionRequirementsDefinitions
     */
    public function getDeployedDecisionRequirementsDefinitions()
    {
        return $this->deployedDecisionRequirementsDefinitions;
    }

    /**
     * Set the value of deployedDecisionRequirementsDefinitions
     *
     * @return  self
     */
    public function setDeployedDecisionRequirementsDefinitions($deployedDecisionRequirementsDefinitions)
    {
        $this->deployedDecisionRequirementsDefinitions = $deployedDecisionRequirementsDefinitions;

        return $this;
    }
}
