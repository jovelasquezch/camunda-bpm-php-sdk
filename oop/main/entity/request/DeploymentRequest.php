<?php

namespace org\camunda\php\sdk\entity\request;

use Illuminate\Support\Arr;

class DeploymentRequest extends Request
{
    protected $deploymentName;
    protected $enableDuplicateFiltering;
    protected $deployChangedOnly;
    protected $deploymentSource;
    protected $tenantId;
    protected $files;
    protected $multipart;

    /**
     * Get the value of deploymentName
     */
    public function getDeploymentName()
    {
        return $this->deploymentName;
    }

    /**
     * Set the value of deploymentName
     *
     * @return  self
     */
    public function setDeploymentName($deploymentName)
    {
        $this->deploymentName = [
            'name' => 'deployment-name',
            'contents' => $deploymentName
        ];

        return $this;
    }

    /**
     * Get the value of enableDuplicateFiltering
     */
    public function getEnableDuplicateFiltering()
    {
        return $this->enableDuplicateFiltering;
    }

    /**
     * Set the value of enableDuplicateFiltering
     *
     * @return  self
     */
    public function setEnableDuplicateFiltering($enableDuplicateFiltering)
    {
        $this->enableDuplicateFiltering =  [
            'name' => 'enable-duplicate-filtering',
            'contents' => $enableDuplicateFiltering
        ];

        return $this;
    }

    /**
     * Get the value of deployChangedOnly
     */
    public function getDeployChangedOnly()
    {
        return $this->deployChangedOnly;
    }

    /**
     * Set the value of deployChangedOnly
     *
     * @return  self
     */
    public function setDeployChangedOnly($deployChangedOnly)
    {
        $this->deployChangedOnly = [
            'name' => 'deploy-changed-only',
            'contents' => $deployChangedOnly
        ];

        return $this;
    }

    /**
     * Get the value of deploymentSource
     */
    public function getDeploymentSource()
    {
        return $this->deploymentSource;
    }

    /**
     * Set the value of deploymentSource
     *
     * @return  self
     */
    public function setDeploymentSource($deploymentSource)
    {
        $this->deploymentSource = $deploymentSource;

        return $this;
    }

    /**
     * Get the value of tenantId
     */
    public function getTenantId()
    {
        return $this->tenantId;
    }

    /**
     * Set the value of tenantId
     *
     * @return  self
     */
    public function setTenantId($tenantId)
    {
        $this->tenantId = $tenantId;

        return $this;
    }

    /**
     * Get the value of files
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set the value of files
     *
     * @return  self
     */
    public function setFiles($files)
    {
        $this->files = $files;

        return $this;
    }

    /**
     * Get the value of files
     */
    public function getMultipart()
    {
        return ['multipart' => $this->files];
    }

    /**
     * Set the value of files
     *
     * @return  self
     */
    public function setMultipart($multipart)
    {
        $this->multipart = $multipart;

        return $this;
    }
}