# camunda-bpm-php-sdk

PHP SDK for camunda BPM

- See http://camunda.github.io/camunda-bpm-php-sdk/ for an introduction and documentation.
- License: Apache License, Version 2.0 http://www.apache.org/licenses/LICENSE-2.0

\*\*\* CHANGELOG
https://packagist.org/packages/datatraffic/camunda-bpm-php-sdk#1.0.1 - Se crea el folk, se reemplaza la integracion de cURL por GuzzleHttp
https://packagist.org/packages/datatraffic/camunda-bpm-php-sdk#1.0.2 - Se corrige el nombre de la clase de CamundaClienteApi a CamundaApiClient, se agrega la instancia de Deployment
